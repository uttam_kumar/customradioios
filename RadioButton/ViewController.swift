//
//  ViewController.swift
//  RadioButton
//
//  Created by Syncrhonous on 14/2/19.
//  Copyright © 2019 Syncrhonous. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var maleSelect: DLRadioButton!
    @IBOutlet weak var femaleSelect: DLRadioButton!
    @IBOutlet weak var otherSelect: DLRadioButton!
    let selection:String = "Female"
    
    @IBAction func radioBtnAction(_ sender: DLRadioButton) {
        //print(sender.titleLabel?.text)
        let s:String = sender.titleLabel!.text!
        print(s)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
       // print(femaleSelect.titleLabel!.text!)
        
        if(selection=="Female"){
            femaleSelect.isSelected = true
        }else if(selection=="Male"){
            maleSelect.isSelected = true
        }else{
            otherSelect.isSelected = true
        }
    }

    
}

